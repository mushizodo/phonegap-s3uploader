var S3Uploader = {
  uploadImage: function(imgUrl, imgName, success, error){
    cordova.exec(
        success,
        error,
        'S3Uploader',
        'uploadImage',
        [imgUrl, imgName]
    );
  }
}

module.exports = S3Uploader;
