/*
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#import "Constants.h"

// Get the following Cognito constants via Cognito Console
NSString *const AWSAccountID = @"996844761672";
NSString *const CognitoPoolID = @"us-east-1:4e3e897a-03ff-4da2-b52c-63ec039cc259";
NSString *const CognitoRoleAuth = @"arn:aws:iam::996844761672:role/Cognito_MyavanaAuth_DefaultRole";
NSString *const CognitoRoleUnauth = @"arn:aws:iam::996844761672:role/Cognito_MyavanaUnauth_DefaultRole";

NSString *const S3BucketName = @"myavana";

//NSString *const S3KeyUploadName = @"upload.jpg";
