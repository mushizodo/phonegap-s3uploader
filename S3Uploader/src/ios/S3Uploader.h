//
//  S3Uploader.h
//  Myavana
//
//  Created by Jungle Nomad on 11/2/14.
//
//

#import <Cordova/CDV.h>

@interface S3Uploader : CDVPlugin

- (void) uploadImage:(CDVInvokedUrlCommand *)command;
- (void) uploadImageWithPath:(NSString *)imagePath andName:(NSString *)imageName;

@end
