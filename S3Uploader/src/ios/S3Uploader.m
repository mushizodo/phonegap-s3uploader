//
//  S3Uploader.m
//  Myavana
//
//  Created by Jungle Nomad on 11/2/14.
//
//

#import "S3Uploader.h"
#import "Constants.h"
#import <AWSiOSSDKv2/S3.h>
#import <ArtisanSDK/ArtisanSDK.h>

@interface S3Uploader ()

@property (nonatomic, strong) NSURL *fileURL;
@property (nonatomic, strong) AWSS3TransferManagerUploadRequest *uploadRequest;

@end

@implementation S3Uploader

- (void) uploadImage:(CDVInvokedUrlCommand *)command {
    
    [ARTrackingManager trackEvent:@"attempt to upload file"];
    
    NSString *imagePath = [command.arguments objectAtIndex:0];
    NSString *imageName = [command.arguments objectAtIndex:1];
    NSLog(@"BLURB! %@", imagePath);
    
    [self uploadImageWithPath:imagePath andName:imageName];
    
    
    // Create an object with a simple success property.
    NSDictionary *jsonObj = [ [NSDictionary alloc]
                             initWithObjectsAndKeys :
                             @"true", @"success",
                             nil
                             ];
    
    CDVPluginResult *pluginResult = nil;
    
    if(imagePath != nil) {
        pluginResult = [ CDVPluginResult
                        resultWithStatus: CDVCommandStatus_OK
                        messageAsDictionary: jsonObj
                        ];
    }else {
        [jsonObj setValue:@"false" forKey:@"success"];
        pluginResult = [ CDVPluginResult
                        resultWithStatus: CDVCommandStatus_ERROR
                        messageAsDictionary: jsonObj
                        ];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) uploadImageWithPath:(NSString *)imagePath andName:(NSString *)imageName {
    self.fileURL = [NSURL URLWithString:imagePath];
    
    NSLog(@"BLURB! %@", [self.fileURL path]);
    
    self.uploadRequest = [AWSS3TransferManagerUploadRequest new];
    self.uploadRequest.bucket = S3BucketName;
    self.uploadRequest.key = imageName;
    self.uploadRequest.body = self.fileURL;
    self.uploadRequest.contentType = @"image/jpeg";
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:self.uploadRequest] continueWithExecutor:[BFExecutor mainThreadExecutor] withBlock:^id(BFTask *task) {
        if (task.error != nil) {
            if( task.error.code != AWSS3TransferManagerErrorCancelled
               &&
               task.error.code != AWSS3TransferManagerErrorPaused
               )
            {
                NSLog(@"ERROR: %@", [task.error userInfo]);
            }
        } else {
            self.uploadRequest = nil;
            NSLog(@"SUCCESS! Image uploaded");
        }
        return nil;
    }];
}

@end
