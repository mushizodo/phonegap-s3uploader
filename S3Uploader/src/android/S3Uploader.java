package com.myavana.cordova.s3;

import java.io.File;

import android.content.Context;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.myavana.cordova.s3.*;
import android.content.Context;
import android.net.Uri;

public class S3Uploader extends CordovaPlugin {
  static CognitoCachingCredentialsProvider sCredProvider;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Context context = this.cordova.getActivity().getApplicationContext();

        if (action.equals("uploadImage")) {
          String img = args.getString(0);
          String imageName = arge.getString(1);
          this.uploadImage(img,imageName,callbackContext);
          return true;
        }
        return false;  // Returning false results in a "MethodNotFound" error.
    }

    public static AWSCredentialsProvider getCredProvider(Context appContext) {
        if(sCredProvider == null) {
            sCredProvider = new CognitoCachingCredentialsProvider(
                    appContext,
                    "996844761672",
                    "us-east-1:4e3e897a-03ff-4da2-b52c-63ec039cc259",
                    "arn:aws:iam::996844761672:role/Cognito_MyavanaUnauth_DefaultRole",
                    null,
                    Regions.US_EAST_1);
            sCredProvider.refresh();
        }
        return sCredProvider;
    }

    private void uploadImage(String img, String imageName, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            Uri uri = Uri.parse(message);
            File image = new File(uri.getPath());
            TransferManager tx = new TransferManager(S3Uploader.getCredProvider(context));
            Upload up = tx.upload(new PutObjectRequest("myavana",imageName, image));
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
